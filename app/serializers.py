from rest_framework import serializers
from app.models import MusicWork, Contributor


class ContributorSerializers(serializers.ModelSerializer):
	class Meta:
		model = Contributor
		fields = ('full_name',)


class MusicWorkSerializers(serializers.ModelSerializer):
	contributors = ContributorSerializers(many=True)
	
	class Meta:
		model = MusicWork
		fields = ('iswc', 'title', 'contributors')
