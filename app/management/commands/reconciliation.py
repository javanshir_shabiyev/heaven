from functools import reduce
import pandas as pd
from django.core.management.base import BaseCommand, CommandError
from app.models import MusicWork, Contributor


class Command(BaseCommand):
	
	@staticmethod
	def add_contributors(item_contributors, work):
		for contributor_full_name in item_contributors.split('|'):
			contributor, contributor_created = Contributor.objects.get_or_create(
				full_name=contributor_full_name)
			
			if contributor not in work.contributors.all():
				work.contributors.add(contributor)
	
	def handle(self, *args, **options):
		
		def contributors_handler(a):
			return list(set(reduce(lambda x, y: x + y, [i.split('|') for i in a])))
		
		try:
			# read from csv
			works_metadata_df = pd.read_csv('works_metadata.csv')
			
			# filter and copy iswc none works to other dataframe
			iswc_nan_works_df = works_metadata_df[works_metadata_df['iswc'].isnull()]
			
			# merge duplicated iswc rows
			iswc_exist_works_df = works_metadata_df.groupby('iswc').agg({
				'title': 'first',
				'contributors': lambda x: '|'.join(contributors_handler(x)),
				'iswc': 'first'
			})
			
			# add to db iswc_exist_works_df items
			for _, iswc_exist_item in iswc_exist_works_df.iterrows():
				iswc_exist_work, iswc_exist_work_created = MusicWork.objects.get_or_create(
					iswc=iswc_exist_item.iswc,
					title=iswc_exist_item.title,
				)
				self.add_contributors(iswc_exist_item.contributors, iswc_exist_work)
				
			# update to db iswc_nan_works_df and merge contributors
			for _, iswc_nan_item in iswc_nan_works_df.iterrows():
				try:
					work = MusicWork.objects.filter(
						title=iswc_nan_item.title,
						contributors__full_name__in=iswc_nan_item.contributors.split('|')).distinct().first()
				
					self.add_contributors(iswc_nan_item.contributors, work)
				except MusicWork.DoesNotExist:
					continue
		except Exception as e:
			raise CommandError(f'Error {e}')
		
		self.stdout.write(self.style.SUCCESS('Successfully closed'))
