from django.core.management.base import BaseCommand
from app.models import MusicWork
from app.utils import index_to_elasticsearch


class Command(BaseCommand):
	
	def handle(self, *args, **options):
		for i in MusicWork.objects.all():
			index_to_elasticsearch(i)
			
		self.stdout.write(self.style.SUCCESS('Successfully closed'))
