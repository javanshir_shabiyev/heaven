from django.conf import settings
from elasticsearch.exceptions import NotFoundError

es = settings.ES


def get_es(iswc):
	res = es.get(index='work', doc_type='work', id=iswc)
	return res


def search_es():
	res = es.search(index='work')
	return [item['_source'] for item in res['hits']['hits']]


def index_to_elasticsearch(instance):

	es.index(index='work',  doc_type='work', id=instance.iswc, body={
		"title": instance.title,
		"iswc": instance.iswc,
		"contributors": [{"full_name": item.full_name} for item in instance.contributors.all()]
	})

