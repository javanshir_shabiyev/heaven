from django.contrib import admin
from app.models import MusicWork, Contributor


admin.site.register(MusicWork)
admin.site.register(Contributor)