FROM python:3.7
MAINTAINER Javanshir Shabiyev <cavanshir.shabiyev@gmail.com>
ENV PYTHONUNBUFFERED 1
COPY  ./ /webapp
WORKDIR /webapp
RUN pip install -U pip && pip install -r requirements.txt && pip install uwsgi