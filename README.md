
## How to run 

1. create the infrastructure needed and start the machines using `docker-compose up`.

2. Now the web application is accessible at `http://localhost:8000` 

3. You should be able to see List operation APIs at `http://localhost:8000/works/` pointing to the  postgres backend 

4. You should be able to see Retrieve API at `http://localhost:8000/works/iswc` pointing to the  postgres backend
 
5. You should be able to see List/Search API at `http://localhost:8000/es/works/` pointing to the  elasticsearch backend 

6. You should be able to see Retrieve/Search API at `http://localhost:8000/es/works/iswc` pointing to the  elasticsearch backend 
  



## Todo:

- [*] `python manage.py reconciliation` reconciliation of works_metadata.csv
- [*] `python manage.py create_index` index all items to elasticsearch
- [*] elasticsearch implementation in case needed low latency
- [*] Dockerfile and docker-compose.yaml

## Projects Uses

- Django 2.2.6
- Elasticsearch 6.4.2
- Postgres latest