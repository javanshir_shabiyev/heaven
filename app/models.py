from django.db import models


class Contributor(models.Model):
	full_name = models.CharField(max_length=255, verbose_name='Full Name')
	
	def __str__(self):
		return self.full_name
	
	class Meta:
		verbose_name = 'Contributor'
		verbose_name_plural = 'Contributors'


class MusicWork(models.Model):
	title = models.CharField(max_length=255, verbose_name='Title')
	iswc = models.CharField(max_length=255, verbose_name='ISWC', unique=True)
	contributors = models.ManyToManyField(to=Contributor, verbose_name='Contributors')
	
	def __str__(self):
		return self.iswc
	
	def save(self, *args, **kwargs):
		if self.id:
			add_es(self)
		return super(MusicWork, self).save(*args, **kwargs)
	
	class Meta:
		verbose_name = 'Music Work'
		verbose_name_plural = 'Music Work'
