from django.shortcuts import get_object_or_404

from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework import status

from app.models import MusicWork, Contributor
from app.serializers import MusicWorkSerializers
from app.utils import get_es, search_es


class MusicWorkListView(viewsets.GenericViewSet, mixins.ListModelMixin):
	authentication_classes = []
	permission_classes = []
	queryset = MusicWork.objects.all()
	
	def list(self, request, *args, **kwargs):

		works = MusicWork.objects.all()
		return Response(MusicWorkSerializers(works, many=True).data, status=status.HTTP_200_OK)


class MusicWorkRetrieveView(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
	authentication_classes = []
	permission_classes = []
	queryset = MusicWork.objects.all()
	
	def retrieve(self, request, *args, **kwargs):
		iswc = kwargs.get('iswc')
		work = get_object_or_404(MusicWork, iswc=iswc)
		return Response(MusicWorkSerializers(work).data, status=status.HTTP_200_OK)


class ESMusicWorkListView(viewsets.GenericViewSet, mixins.ListModelMixin):
	authentication_classes = []
	permission_classes = []
	queryset = MusicWork.objects.all()
	
	def list(self, request, *args, **kwargs):
		works = search_es()
		return Response(works, status=status.HTTP_200_OK)


class ESMusicWorkRetrieveView(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
	authentication_classes = []
	permission_classes = []
	queryset = MusicWork.objects.all()
	
	def retrieve(self, request, *args, **kwargs):
		iswc = kwargs.get('iswc')
		work = get_es(iswc)
		return Response(work.get('_source'), status=status.HTTP_200_OK)
