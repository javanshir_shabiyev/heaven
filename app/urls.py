from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from .views import *

urlpatterns = [
    path('works/', MusicWorkListView.as_view({"get": 'list'})),
    path('works/<path:iswc>', MusicWorkRetrieveView.as_view({"get": 'retrieve'})),
	
	path('es/works/', ESMusicWorkListView.as_view({"get": 'list'})),
    path('es/works/<path:iswc>', ESMusicWorkRetrieveView.as_view({"get": 'retrieve'})),
  ] + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS[0])
